struct Dicionario {
 1: required string tipo;
 2: required i64 num_ramal;
 3: required string objectid;
 4: required string endereco;
 5: required string nome;
 6: required string mac;
 7: required string modelo;

}

service Ramais {
    list<Dicionario> get_ramais()
    list<Dicionario> get_ramal(1:string ramal)
    map<string,string> delete_ramal(1:string ramal)
    map<string,string> post_ramal(1:string tipo, 2:string ramal, 3:string endereco, 4:string nome, 5:string mac, 6:string modelo)
    map<string,string> put_ramal(1:string tipo, 2:string ramal, 3:string endereco, 4:string nome, 5:string mac, 6:string modelo, 7:string id)
}
