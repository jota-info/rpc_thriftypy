# -*- coding: utf-8 -*-
"""Servidor responsável por efetuar os CRUDS

    na tabela de ramais, e repassar as informações
    de alterações para o serviço de mensageria
    do rabbitmq
"""

# Importações fundamentais

# Necessários para o mongo
from pymongo import MongoClient
# from bson.objectid import ObjectId

# Necessários para o thriftpy
from thriftpy.rpc import make_server
import thriftpy

# Necessários para o rabbitmq
import pika
# Fim das importações

# Carrega o contrato RPC do arquivo extrafunctions.thrift
extrafunctions_thrift = thriftpy.load(
    "extrafunctions.thrift",
    module_name="ramais_thrift"
)


class Ramais(object):
    """Classe CRUD de ramais. Também responsável enviar as

        mensagens de mudanças para outros clientes.
    """

    def __init__(self):
        """Inicializador da classe"""
        # Inicialização do mongodb
        self.mongo_client = MongoClient()
        self.db = self.mongo_client.asterisk

        # Inicialização do serviço de mensageria
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(host='localhost'))
        # Definição de uso de canal para o rabbit
        self.channel = self.connection.channel()
        # Escolha do tipo de mensageria, nesse caso o "fanout" torna o módulo
        # logs um tipo de broadcast. As mensagens não serão
        # apagadas instantâneamente nem fornecidas para o primeiro que receber
        # apenas, mas para todos os clientes disponíveis
        self.channel.exchange_declare(exchange='logs', type='fanout')

    def __broadcast(self, message):
        """Método privado responsável por enviar uma mensagem

            ao servidor de message queue rabbitmq
        """
        # Publicação de mensagem no message queue
        self.channel.basic_publish(
            exchange='logs', routing_key='', body=message)

    def get_ramais(self):
        """Retorna todos os ramais

            e chama o método privado __broadcast
            para enviar uma mensagem anunciando
            que ouve uma consulta em massa
        """
        cursor = self.db.ramais.find({})
        ramais = []

        for doc in cursor:
            ramais.append(
                extrafunctions_thrift.Dicionario(
                    doc['tipo'],
                    int(doc['ramal']),
                    str(doc['_id']),
                    doc['endereco'],
                    doc['nome'],
                    doc['mac'],
                    doc['modelo']
                ),
            )
        # Chamada do método privado
        self.__broadcast("Foram consultados todos os ramais")
        # Retorno oferecido ao cliente principal
        return ramais

    def get_ramal(self, ramal):
        """Retorna informações específicas de um

            ramal escolhido
        """
        cursor = self.db.ramais.find_one({'ramal': ramal})
        ramais = []
        ramais.append(
            extrafunctions_thrift.Dicionario(
                cursor['tipo'],
                int(cursor['ramal']),
                str(cursor['_id']),
                cursor['endereco'],
                cursor['nome'],
                cursor['mac'],
                cursor['modelo']
            )
        )
        # Chamada do método privado
        self.__broadcast("Foi consultado os detalhamentos de um ramal")
        # Retorno oferecido ao cliente principal
        return ramais

    def delete_ramal(self, ramal):
        """Elimina o ramal passado como parâmetro"""
        self.db.ramais.delete_one({'ramal': ramal})
        # Chamada do método privado
        self.__broadcast("Foi deletado um ramal")
        # Retorno oferecido ao cliente principal
        return {"Resultado": "Ramal eliminado"}

    def post_ramal(self, tipo, ramal, endereco, nome, mac, modelo):
        """Cria um novo ramal"""
        new = {
            'tipo': tipo,
            'ramal': ramal,
            'endereco': endereco,
            'nome': nome,
            'mac': mac,
            'modelo': modelo
        }
        self.db.ramais.insert_one(new)
        # Chamada do método privado
        self.__broadcast("Foi inserido um novo registro")
        # Retorno oferecido ao cliente principal
        return {"Resultado": "Ramal criado"}

    def put_ramal(self, tipo, ramal, endereco, nome, mac, modelo):
        """Cria um novo ramal"""
        self.db.ramais.update_one({'ramal': ramal}, {'$set': {
            'tipo': tipo,
            'ramal': ramal,
            'endereco': endereco,
            'nome': nome,
            'mac': mac,
            'modelo': modelo
            }})

        # Chamada do método privado
        self.__broadcast("Foi alterado um ramal")
        # Retorno oferecido ao cliente principal
        return {"Resultado": "Ramal alterado"}


def main():
    """Função principal"""
    # Roda esse servidor escutando na porta 7000
    server = make_server(
        extrafunctions_thrift.Ramais,
        Ramais(),
        '127.0.0.1',
        7000
    )
    server.serve()


if __name__ == "__main__":
    main()
