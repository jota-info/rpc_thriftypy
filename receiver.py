# -*- coding: utf-8 -*-

"""Cliente da mensageria

    Representando qualquer microserviço que necessitasse
    das informações dos ramais em tempo real para evitar
    conflitos.

    Nesse caso, apenas são recebidas as mensagens, porém
    não são tomadas ações
"""

# Importações necessárias

# Importação do serviço de mensageria
import pika

# Estabelecimento da conexão e canal
# Embora tenha sido realizado no server,
# é considerado boa prática pela documentação
# do rabbit a repetição da conexão e canal
connection = pika.BlockingConnection(pika.ConnectionParameters(
	host='localhost'))
channel = connection.channel()
channel.exchange_declare(exchange='logs',
	type='fanout')

# Define que será ouvido qualquer mensagem que esteja
# disponível no serviço fornecido pelo rabbit, e a flag
# exclusive, indica que após fechada a conexão por completo
# as mensagens serão zeradas
result = channel.queue_declare(exclusive=True)

# Deixa o rabbitmq responsável por definir a queue
# que será usada no server
queue_name = result.method.queue

# Define um binding entre a queue e a exchange
channel.queue_bind(exchange='logs',
	queue=queue_name)

print(' [*] Esperando algo acontecer. Para parar tecle CTRL+C')


def callback(ch, method, properties, body):
	"""Callback responsável por verificar as

		mensagens fornecidas pelo rabbitmq
	"""
	print(" [x] %r" % body)

# Definições da queue que o callback
# estará analizando
channel.basic_consume(callback,
	queue=queue_name,
	no_ack=True)

# Mantém a callback em funcionamento como
# um consumidor dos dados mantidos
# na fila de mensageria
channel.start_consuming()
