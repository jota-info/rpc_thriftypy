# RPC thriftypy - Para arquiteturas de microserviços!

##Projeto para estudos a respeito de:##
* Apache Thrifty RPC, fazendo uso do Thriftypy
* Rabbitmq - Sistema de mensageria
 
# Porque estou tentando usar Thrifty RPC e Rabbitmq?
## Vamos pensar a respeito de microserviços!
Vamos pensar um pouco a respeito dessa arquitetura chamada microserviços. De forma estúpidamente resumida,
a arquitetura de microserviços aborda um grande sistema composto de diversos serviços dedicados. Em uma loja
virtual por exemplo, poderíamos ter um serviço que se dedica apenas ao mostruário de produtos e informações, 
outro que se dedica apenas a vendas e pagamentos, e ainda outro que trata de gerir os usuários cadastrados.
	
Cada microserviço, contém sua própria linguagem, banco de dados e bibliotecas. Eles são completamente isolados
uns dos outros. Porém todos juntos, formam um só sistema(supersistema?). Isso dá liberdade para uso de diversas
técnologias para sanar problemas específicos. Por exemplo, um mostruário de todos os produtos da loja, seria mais
eficiente com um banco de dados nosql como mongodb, que comprovadamente faz consultas muito mais rápidas que um banco
de dados padrão sql. Porém para vendas e pagamentos, talvez um banco nosql não seja tão simples de gerir, e o usuário
não se importa se demorar um pouco mais para a compra e pagamento serem realizados, desde que haja certeza que não haverá
problemas de segurança com o número do cartão, com o pagamento e etc. 
	
Essa liberdade que se tem ao utilizar uma arquitetura de microserviços pode ser uma grande aliada, e uma grande 
dor de cabeça, caso sejam misturadas muitas tecnologias, é necessário bom senso no uso.
	
Os microserviços precisam de certa forma se comunicar, embora sejam mini-sistemas separados, em alguns casos são necessárias
informações que só outro microserviço terá. Cada microserviço, é na verdade uma api nesse caso de estudo. Cada API recebe
informações em json e retorna um json. Por exemplo, o microserviço de compra da loja virtual, precisa saber qual produto
o usuário escolheu do serviço de mostruário. Ao clicar em comprar, esse microserviço de mostruário enviará um JSON para
o serviço de compras, para continuar o processo. 
	
### Ai entra o Rabbitmq(mensageria)...
	
O cliente efetivou a compra no serviço de compras, e esse serviço vai eliminar uma unidade no estoque banco de dados dele.
Ok! Tudo certo! Não, espera ai... os bancos de dados em microserviços são separados não são? Sim! Por isso é necessário um sistema de 
mensageria, conhecido por ai como "message queue". Não gosto muito de inglês aqui no meu repositório, embora eu saiba inglês,
minha intenção é que esses repositórios públicos sejam acessíveis para outras pessoas aprenderem em português, afinal,
porque que conteúdo bom não pode ser Brasileiro? Em fim! Voltando, é ai que entra a mensageria. 

De forma simples, existe uma fila de mensagens, e ao tirar um produto do estoque, é disparada uma mensagem para o servidor
de mensagens rabbitmq, e este servirá de broadcast, espalhando a notícia para todos os outros microserviços, alertando
da necessidade de uma alteração em seu banco, para que todos andem em harmonia, oh que bonito!

### Lindo! Mas e o RPC?
	
O RPC é uma chamada de procedimento remoto(sim, do famoso inglês REMOTE PROCEDURE CALL), basicamente você chama de forma
remota um procedimento que está em outro local(outro serviço ou até mesmo outra máquina(virtual ou não)). Ok, mas para que?
Afinal não estamos fazendo serviços assim de certa forma? 

Quando implemento um estudo de RPC, penso num procedimento muito específico, que necessite de uma máquina fazendo o determinado
serviço, ou de uma linguagem de mais baixo nível, ou seja em uma plataforma completamente diferente. Aqui nesse repositório nós 
temos apenas uma simulação. Veja a imagem abaixo:

![Microserviços.png](https://bitbucket.org/repo/eBpbeM/images/2325890527-Microservi%C3%A7os.png)

Agora ficou claro? Sei que um CRUD não é uma tarefa pesada para se chamar um rpc, mas é apenas uma simulação.
Nesse projeto, temos um frontend, que é o que o usuário vê, que por acaso, não me preocupei em fazer ainda. Lembrando que um
microserviço não precisa ser necessariamente uma API, ele pode conter o próprio front, no meu caso, são APIs. Então tenho dois microserviços, um deles é um CRUD. Minha API do lado esquerdo é responsável por entender o que veio do Front, e pedir para que o RPC
acima dela, faça o serviço pesado e atualize o banco, além de avisar a mensageria que algo mudou. O outro microserviço a direita, que
tem muito haver com a história, está interessado em saber se alterou algo no banco, ao saber que sim ele atualiza o próprio banco, e
também informa caso tenha mudado alguma coisa para a mensageria, fazendo o trabalho inverso.

## Isso tudo quer dizer que os Bancos de Dados são iguais dos dois lados?

Não! Pode ser que um dado seja importante no outro serviço, mas não que há duplicação de banco de dados, saiba separar bem essas
situações. Se os bancos de dados forem iguais, significa que você está provavelmente fazendo duplicação de lógica de aplicação,
o que é justamente o que a arquitetura de microserviços busca eliminar. Espero que tenha aprendido coisas úteis aqui, lembrando
que você pode usar meu repositório a vontade. Dúvidas sobre o assunto aqui: j.h.o.junior12@gmail.com

Abaixo as instruções de uso.


### Ramais ###

* O projeto é um CRUD de ramais
* O objetivo é a função client ser uma api backend, que chama os métodos remotos do server
* O server também é responsável por enviar mensagens ao rabbitmq
* A aplicação receiver.py, fica escutando do rabbitmq se existem novas mensagens, simulando uma outra API
* Não são tomadas ações no receiver.py, por ser apenas uma simulação exemplo, o importante é ela receber as mensagens do server em tempo real

### Instalação ###

* Leia atentamente ao requirements.txt, para saber as dependências
* Em todas as aplicações está sendo usado Python3
* É necessário instalar o rabbitmq(https://www.rabbitmq.com/download.html)

### Testar app ###

* Abra um terminal e inicie o rabbitmq
* Abra outro terminal e inicie o receive.py, ele ficará aguardando mensagens entrarem
* Abra um terminal e inicie o server.py, e outro terminal para iniciar o client.py
* Você pode testar via curl, ou usando o postman do Chrome, ou fazer uma aplicação javaScript
* Vale lembrar novamente, que todos os servidores e arquivos py devem ser iniciados com Python versão 3.

### Como usar o curl ###

* Listar todos os ramais
	```curl http://localhost:5000/ramais```
* Lista um ramal
	```curl http://localhost:5000/ramais/{Número do ramal}```
* Adicionar um novo ramal
	```curl -X POST -H "Content-Type: application/json" -d '{"tipo": "SIP", "ramal": "7777", "endereco":"192.168.xx.xxx", "nome":"Exemplo de Carvalho", "mac":"MACADRESS", "modelo":"ABCEXEMPLO"}' http://localhost:5000/ramais```
* Deletar um ramal
	```curl -X DELETE http://localhost:5000/ramais/{Número do ramal}```
* Alterar um ramal
	```curl -X PUT -H "Content-Type: application/json" -d '{"tipo": "SIP", "ramal": "7777", "endereco":"192.168.xx.xxx", "nome":"Exemplo de Carvalho", "mac":"MACADRESS", "modelo":"ABCEXEMPLO"}' http://localhost:5000/ramais/{Número do ramal}```