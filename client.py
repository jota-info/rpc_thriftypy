# -*- coding: utf-8 -*-
"""Cliente/Servidor web

    Como servidor, é responsável em entender as
    requisições Json do cliente frontend, e pedir
    ao server "motor/principal" a resposta devolvendo
    ao cliente frontend igualmente em Json.

    Como cliente, envia uma requisição ao motor,
    e aguarda a resposta.
"""

# Importações necessárias

# Importações para o Flask e sua extensão Flask-restful
from flask import Flask, jsonify
from flask_restful import Resource, Api, reqparse
from flask.views import MethodView

# Importações para o thrifty
from thriftpy.rpc import make_client
from thriftpy.protocol.json import struct_to_json
import thriftpy

# Inicialização da aplicação
app = Flask(__name__)
api = Api(app)

# Inicialização do módulo ramais_thrift, fazendo
# uso do contrato extrafunctions.thrift
extrafunctions_thrift = thriftpy.load(
    "extrafunctions.thrift",
    module_name="ramais_thrift"
)


def serializer():
    """Serializador dos dados que são recebidos como parâmetros

    utilizando o formado Json para a coleção ramais
    """
    parser = reqparse.RequestParser()
    parser.add_argument('tipo', type=str)
    parser.add_argument('ramal', type=str)
    parser.add_argument('_id', type=str)
    parser.add_argument('endereco', type=str)
    parser.add_argument('nome', type=str)
    parser.add_argument('mac', type=str)
    parser.add_argument('modelo', type=str)
    return parser.parse_args()


class Ramais(MethodView):
    """Classe destinada a ações para todos os ramais"""
    def __init__(self):
        """Estabelece as conexões"""
        # Solicitação do serviço Ramais do motor principal
        # na porta 7000
        self.client = make_client(
            extrafunctions_thrift.Ramais,
            '127.0.0.1',
            7000
        )

    def get(self):
        """Retorna todos os ramais"""
        # Retorna o resultado do método do motor chamado
        # get_ramais
        return [struct_to_json(x) for x in self.client.get_ramais()]

    def post(self):
        """Adiciona um novo ramal"""
        # Recebe argumentos serializados
        args = serializer()
        # Faz a chamada da função post_ramal
        # do motor, entregando os parâmetros
        # recebidos pelo serializer
        return jsonify(self.client.post_ramal(
            args['tipo'],
            args['ramal'],
            args['endereco'],
            args['nome'],
            args['mac'],
            args['modelo'],
        ))


class Ramal(Ramais, MethodView):
    """Classe destinada a operações com ramal específico

    	tomando por herança as inicializações da classe
    	Ramais
    """

    def get(self, ramal):
        """Retorna o ramal passado por parâmetro"""
        try:
            # Caso o server retornar as informações do ramal
            # então é passado para o cliente
            return struct_to_json(self.client.get_ramal(ramal)[0])
        except:
            # Caso o server retorne um erro, certamente o ramal
            # Não existe
            return jsonify({"Resposta": "Ramal não encontrado!"})

    def delete(self, ramal):
        """Elimina o ramal passado por parâmetro"""
        # Chamada da função delete_ramal do server
        return jsonify(self.client.delete_ramal(ramal))

    def put(self, ramal):
        """Altera o ramal passado por parâmetro"""
        # Situação se assemelha ao post da classe pai
        args = serializer()
        return jsonify(self.client.put_ramal(
            ramal,
            args['tipo'],
            args['ramal'],
            args['endereco'],
            args['nome'],
            args['mac'],
            args['modelo'],
        ))

def main():
    # Definição das urls das classes
    api.add_resource(Ramais, '/ramais')
    api.add_resource(Ramal, '/ramais/<ramal>')
    app.run(debug=True)

if __name__ == '__main__':
    """Função principal"""
    main()
